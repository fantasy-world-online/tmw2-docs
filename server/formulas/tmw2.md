# General Balance Formulas for TMW2

As usual, general balance formulasare as generic as they could. Tweak as needed.

I repeat, **TWEAK AS NEEDED**. Just don't go too far from them or you'll end up
with a broken balance.

## Monster Experience Gain

* Level 1: total 1 mobs individual 3 xp (1.450)
* Level 2: total 3 mobs individual 5 xp (1.450)
* Level 3: total 8 mobs individual 5 xp (1.450)
* Level 4: total 16 mobs individual 4 xp (1.450)
* Level 5: total 26 mobs individual 7 xp (1.450)
* Level 6: total 39 mobs individual 8 xp (1.450)
* Level 7: total 56 mobs individual 9 xp (1.450)
* Level 8: total 77 mobs individual 9 xp (1.450)
* Level 9: total 101 mobs individual 10 xp (1.450)
* Level 10: total 129 mobs individual 11 xp (1.450)
* Level 20: total 667 mobs individual 21 xp (1.450)
* Level 30: total 1767 mobs individual 57 xp (1.450)
* Level 40: total 3597 mobs individual 148 xp (1.475)
* Level 50: total 6724 mobs individual 295 xp (1.525)
* Level 60: total 11871 mobs individual 579 xp (1.575)
* Level 70: total 20071 mobs individual 952 xp (1.625)
* Level 80: total 32389 mobs individual 1783 xp (1.650)
* Level 90: total 47806 mobs individual 3635 xp (1.650)
* Level 100: total 66308 mobs individual 4976 xp (1.650)

Formula:
```py
    hxp=exp[lv]
    if not mb.boss:
        if lv > 35:
            fx=1.45+((lv-35)/200.0)
        else:
            fx=1.45
        if fx > 1.65:
            fx=1.65

        # Aggressive, assistant and fast mobs yield more exp
        # Slow monsters yield less exp, because they're easier to kill on any playstyle
        if magr:
            fx-=0.1
        if mass:
            fx-=0.08
        if mv < 200:
            fx-=0.02
        if mv > 500:
            fx+=0.02

        # 1 ^ anything = 1, so we need a better rule
        if lv != 1:
            lxp=exp[lv-1]/(lv**fx)
        else:
            lxp=3 # 3~5 is fine
    # Boss are BOSS. It gives roughly 0.1% from lvlup xp.
    else:
        lxp=exp[lv]/1000
```

## Monster HP

Please take in account the weapon damage formulas.

```py
    # HP: Each 10 levels give you 100 extra weapon damage
    # I expect from 6 to 14 hits to kill
    # You deliver in average two hits per second
    # Mobs deliver one hit each four seconds (and that's 1hand advantage)
    lhp=lv*20*6+lv*(vi/100)
    hhp=lv*20*14+lv*(vi/100)
    if mb.boss:
        lhp*=1.4
        hhp*=1.8
    if "slime" in mb.name.lower():
        lhp*=0.6
        hhp*=0.6
    if attackrange > 1:
        lhp*=0.9
        hhp*=0.9
```

## Monster Attack

This one is tricky because it was calculated without defense.
But HPTable formula seems to be doing the trick thus far.

```py
    # HPTable Formula: 400+(x*50)
    # MASTERED
    lat=(lv*40+lv**1.2+lv*(st/100))/ar
    hat=(lv*40+lv**1.5+lv*(st/100))/ar
    # Special Cases
    if mb.boss:
        lat*=1.2
        hat*=1.4
    if "slime" in mb.name.lower():
        lat*=0.3
        hat*=0.3
    # Attack is DPS. So we must adjust to be a damage per second
    lat*=(ad/1872)
    hat*=(ad/1872)
    # Formula is not reliable
    lat/=3
    hat/=3
    # Consider Aggressive and Assist mobs
    if magr:
        lat*=0.78
        hat*=0.78
    if mass:
        lat*=0.89
        hat*=0.89
```

## Weapon Damage

This should be capped at 11/level because that's how mobHP formula works.

### Melee Weapons
```
    1 hand weapon: Level * 8
    2 hand weapon: Level * 10
    Crafting items: Critical Damage +30%
    2 hand weapon: Dex/Agi -(Lvl/10), Def -(Lvl*0.8), Crit +(Lvl/5)
    HH/Rare/Craft/Lucky bonus: Multiplier +1 (except Kanabo & Centaur Spear)
```

Please keep in mind that a critical hit disregards flee rate (but not blocking).
Critical hits also disregards defense. This means that, while 2 hand users
suffer from less dex, their additional critical rate makes up for that.
With the added bonus that critical ignores mob evasion!

TL;DR - Two hand swords can hit Tortugas. Critical here is different.
Note: Perfect Blocking (from shields) is absolute. 2h swords have no power against.

Centaur Spear have increased range, so damage is not nominal.

### Magic Weapons

Wands need total rebalance yet. Magic was poorly tested.

```
    Magic Attack = Lv * 7+(random multiplier from 0 to 1)
    Each 2 levels, they add 1 to maximum SP pool.
    
    There are no other bonus atm, but we have about 30 available to use.
```

### Ranged Weapons

They are divided in two grand groups: Arrow-based and Bullet-based.

```
    Redesign for bows will follow the following formula:
    lv×(8+(lv÷100))
```
In other words: multiplier is `8 + (level/100)`
So for a level 60 bow, multiplier will be 8.6

```
    Rifles: 2 hand sword damage
    Revolvers: 4/5 from 1 hand sword damage
    Shotgun: 3/5 from 1 hand sword damage
    Gatling Gun: No damage applied
```

Firearms need a total rewrite. I haven't tested them properly either, just like magic.

## Ammo Box Prices

The arrow prices in the ammo boxes is:

```
    300+200×0,05×(dmg-10)
```

This way, Tolchi Arrow (weakest) cost 300 Gp. You can get 200 GP by selling them.
This shall be enough to prevent profit from buying packs and selling arrows.

Each arrow cost 0,05 GP the damage (minus Tolchi Arrow). However, very powerful
arrows might have special effects or need to have less damage to don't overpower.

## Quivers Status

Maximum quiver attack speed bonus is 35% because otherwise it gets faster than
melee and this is strictly forbidden.

Required agility is 70% from base level - if you are distributing your points
evenly across agility and dexterity, this shouldn't be a problem.

## Armor Defense Distribution

![Defense Scaling in TMW2](server/formulas/Defense.png)

As you can see, defense use a non-linear scale, ultimately capping around 10.0%
damage received as you near absurd amounts of defense (eg. a million).

Therefore, we used 800 defense (which means 60% damage reduction) as reference for
level 100 and baked the following formula:

```
Def=(Lv**1.255)*2.5
```

You'll notice it won't give exactly 800 defense at level 100 - it gives a bit more.
That's because rounding.

That is TOTAL LEVEL MAXIMUM DEFENSE. When reaching higher level equipment, consider
giving VIT DEF (aka. Raw Damage Reduction or Hard DEF).

Take in mind that **critical attacks disregard defense**, and therefore, melee users,
specially tankers, will really need critical defense.

The level maximum defense is divided roughly as follows, remembering that attribute
*Heavy* stands for armor which *lowers movement speed*.

```
Heavy Armor - 40%
Light Armor - 30%

Heavy Helmet - 15%
Light Helmet - 10%

Leggings - 10%

Shields - 25%
Gloves - 5%
Boots - 5%
```

This totals to 100% when using heavy armor with shield. Archers, mages, two handed
weapon users will therefore have significant less defense.

In general, archers and mages cannot/shouldn't use heavy armor or shields, so they have
only 60% defense from planned.

Users with two hand swords can use the heavy armor (and probably will use it, too).
They will enjoy 75% defense.

If for some reason a melee fighter decide to don't use the heavy armor because
speed, they'll still have 85% defense from the reference, meaning that not using
a shield is worse than not using heavy armor.

