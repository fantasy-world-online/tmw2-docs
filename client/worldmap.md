# World Map

This provides a quick reference to the main monsters on each area, along a brief description.

*Warning*. This is a fan-made content. Real data might differ. Please consult the Monster Information Pages on our wiki for more accurate information.

![Worldmap](worldmap.jpg)

# Candor Island

## 1. Candor Island

The Noob Area

* Lv 1 - Piou
    * HP: 120
    * ATK: 23
    * EXP: 2
    * Desc: A small yellow bird
* Lv 4 - House Maggot
    * HP: 480
    * ATK: 30
    * EXP: 6
    * Desc: An aggressive maggot. Be careful with packs.
* Lv 5 - Maggot
    * HP: 600
    * ATK: 69
    * EXP: 7
    * Desc: A vermin plague, terror of fair citzens.
* Lv 5 - Candor Scorpion
    * HP: 600
    * ATK: 75
    * EXP: 8
    * Desc: A weakly magical scorpion which only appears at Candor Island.
* Lv 10 - Scorpion
    * HP: 1201
    * ATK: 138
    * EXP: 12
    * Desc: A dangerous, sightly golden scorpion, known for harming poor children.
* Lv 15 - Mana Bug
    * HP: 2704
    * ATK: 70
    * EXP: 26
    * Desc: Its magic powers turned it into a dangerous creature. Usually peaceful, they'll attack anyone once provoked.
* Lv 26 - **Saxso Ghost**
    * HP: 4776
    * ATK: 142
    * EXP: 84
    * Desc: What remained from Candor former Mayor, Saxso. How he got cursed into a ghost remains a mistery. He is a **Boss**.

# Tonori Region

## 2. Tulimshar City

Without death penalty, and world's largest trading spot, the most resistant city against a monster attack.

* Lv 5 - Maggot
    * HP: 600
    * ATK: 69
    * EXP: 7
    * Desc: A vermin plague, terror of fair citzens.
* Lv 6 - Little Blub
    * HP: 800
    * ATK: 44
    * EXP: 10
    * Desc: Round, small, peaceful, loves to attack in packs.
* Lv 9 - Croc
    * HP: 1900
    * ATK: 124
    * EXP: 18
    * Desc: Small crab-like sea creature, resistant to damage.
* Lv 14 - Duck
    * HP: 1282
    * ATK: 60
    * EXP: 15
    * Desc: Ducks are know for rapid, vicious attacks.
* Lv 14 - Blub
    * HP: 2801
    * ATK: 159
    * EXP: 28
    * Desc: Mother of all little blubs
* Lv 24 - Toppy Blub
    * HP: 5043
    * ATK: 288
    * EXP: 46
    * Desc: Has a sense of fashion and uses a slime hat. Dangerous creature commonly hunted for quality fishing baits.
* Lv 40 - Giant Maggot
    * HP: 4813
    * ATK: 600
    * EXP: 127
    * Desc: They are terribly slow, but once they land a hit, they show to players the real importance of hit'n'run on this game.

## 3. Tulimshar Mines

The best place to get Rubies and noob equipment.

* Lv 7 - Ratto
    * HP: 500
    * ATK: 81
    * EXP: 9
    * Desc: Experient thieves of all loots
* Lv 15 - Cave Maggot
    * HP: 1000
    * ATK: 100
    * EXP: 19
    * Desc: The harsh environment made a powerful, aggressive maggot.
* Lv 19 - Red Scorpion
    * HP: 2283
    * ATK: 236
    * EXP: 24
    * Desc: Attacks in pack, do not provoke them when they're in group.
* Lv 30 - Cave Snake
    * HP: 3607
    * ATK: 320
    * EXP: 53
    * Desc: With a lamp at their heads to make walking easier.
* Lv 50 - Black Scorpion
    * HP: 6309
    * ATK: 618
    * EXP: 404
    * Desc: Dark as death, and extremely aggressive. Their speed allows people to run away, otherwise, death awaits!

## 4. Desert Canyon

The desert is deadly. This place is infested with dangerous monsters and bandits.

* Lv 22 - Desert Bandit
    * HP: 2643
    * ATK: 192
    * EXP: 26
    * Desc: They are not thieves, they are bandits. If you don't give your items to them, they'll rush to kill you.
* Lv 24 - Sarracenus
    * HP: 2883
    * ATK: 209
    * EXP: 34
    * Desc: Rogue Raijin, knows some skills. They fight with dare hands and are great at killing and looting drops.
* Lv 35 - Desert Log Head
    * HP: 4211
    * ATK: 392
    * EXP: 84
    * Desc: The only reliable source of wood in the desert.
* Lv 40 - Snake
    * HP: 4813
    * ATK: 305
    * EXP: 214
    * Desc: Aggressive, dangerous, furious, poisonous snake.
* Lv 50 - Black Scorpion
    * HP: 6309
    * ATK: 618
    * EXP: 404
    * Desc: Dark as death, and extremely aggressive. Their speed allows people to run away, otherwise, death awaits!


# Deep Deserts

## 4. Halinarzo Town

It's just a town. No penalty, as usual

## 5. Landbridge

Usually flooded, this is a dangerous area.
North of it is the Eternal Swamps.

# Argaes Region

## 6. LoF Region
## 7. Woodlands
## 8. The Ice Pass

# Kazei Region

## 9. Romantic Place
## 10. Winterlands
## 11. Nivalis Port
## 12. Nivalis Caves
## 13. Frostia Caves
## 14. Frostia Cliffs

