# TMW2: Moubootaur Legends
## Jesusalva's ToDo List

## Urgent Requests

* [ ] Candor Visual Redesign
* [ ] The Academy
    * [ ] Maps
    * [ ] Main Class Redesign
    * [ ] Subclass Redesign
    * [ ] Magic Council Redesign
* [x] Mercenary System Redesign
    * [x] Up to 7* is it?
        * See properly how level is distributed among mercenaries
    * [x] More mercenary boxsets
        * [x] Fixed-star-rating boxsets
        * [ ] Superior boxsets (never a weak mercenary - for events)
        * [x] Inferior boxsets (never a strong mercenary - for quests)
        * Weak/Strong should be based on player level.
        * If you got a weak or a strong, based on luck.
    * [x] Ranged mercenaries
    * [x] Event rewards vs Daily Quest rewards
        * Maybe in Tulimshar Guard House, and make it work like a guild?
        * [x] Random-monster-killing daily quest (based on your level)
    * [ ] Explain how system works - ie. group with Lua, Player Quest
        * Maybe even give a fixed-star-rating 1* mercenary
* [ ] Setzer
    * [ ] Monster Potion - dyed, special avatar
    * [ ] Monster Potion Item
    * [ ] Monster Potion Making at Nivalis
    * [ ] Short Sword *somewhere* besides HH Store.
    * [ ] Pour the potion over the short sword... (How?)
* [x] Blue Sage Quest
    * [x] Blue Sage Maps
    * [x] Blue Sage Quest Special Reward
* [ ] Frostia
    * [ ] AF King
        * AFK Cap (all ready, just push)
    * [ ] Replace Desert Bow by Elfic Bow and make it available.
* [ ] Lilit
    * [ ] Real Estate
    * [x] Leather Trousers
* [ ] Mechanics
    * [x] Warp Crystals
    * [x] town.txt - Users coming back from death might need LOCATION$ reset
    * [ ] NPC_<Ele>ATTACK to all relevant monsters (overpopulating mob skill db)
* [ ] Monsters
    * [x] Evil Goblin
    * [x] Terranite and Terranite King
    * [ ] Spider
    * [ ] Flowers
    * [x] Ice Sprite (TMW-DE)
    * [ ] Cursed Soldiers (Micksha)
    * Concepts:
    * [ ] THE MOUBOOTAUR
    * [ ] Scorpion King
    * [ ] Mechanical Beigns (Assault Weapons, (Mobile) Fortifications, Siege Crossbows...)
    * [ ] Sea Monsters (Hydra, Tentacles, Octopus, Sea Snake, Tritans, Nagas, Merfolk, etc.)
    * [ ] Mimics?
    * [ ] Generic Cave Monster (strange creatures without counter-part on real world)
    * [ ] Harpies, Eagles, Birds, Gryphons, etc.
    * [ ] Magical beings: Elementals (Fire elemental, Water elemental, etc.), Summon
          Gates, Homunculuses, Woses, Golems (small, big, fat, thin, with swords, with lazers, etc.) ...
    * [ ] Insects and Insectoids (eg. ants, bees, annoying flies and swarms, lizards,
          Alligators, etc.)
    * [ ] Ninjas, Gladiators, etc. for the PvP arena - It should work, if we only
          have one player for the PVP Duel, no need to cancel the fight - just spawn
          a few Gladiator monsters and enjoy :>
    * [ ] Dragons (Red Dragon, Smoke Dragon, etc. - Green Dragons being the weakest)
        * [ ] Wyverns and Drakes can be done, too

* [ ] Other
    * [ ] Guild - Warp object
    * [ ] Guild - Kamelot Castle
        * [ ] Lightbringer - only gm - guild_lv minutes - rent system
    * [x] Braknar Shield
        * [x] Add Baktar
        * [x] Add Khafar
    * [ ] Katzei Family
        * [ ] Black Cat Boots ( serverdata#47 )
        * [ ] Cat Ears ( serverdata#47 and LOW PRIORITY )
    * [x] Craft Systems
        * [x] Alchemy Recipes
        * [x] Smith Recipes
        * [ ] Smith forge to houses and rental
        * [ ] Blueprint improvement
    * [ ] Block @spawn of Moubootaur or Monster King
    * [ ] Automatic Treasure Chest monster spawn in dungeons (maybe with an event so
          previous spawns are killed each cycle, and population is kept roughly based
          on player count) - Or mimic :> Feeling lucky? :>
    * [ ] Traps, poison makers for poison arrows: Buy common Arrow, and ask a Poison
          Maker Player to upgrade them! (Make normal arrows cheaper, too)
* [ ] Guilds
    * [ ] Guild GvG
        * [x] Alchemy Recipes
        * [ ] Smith Recipes

* [ ] Experience Orb items
    * Treasure chests drop those?
* [x] Light Platemail
    * Saulc said nothing - do whatever
* [ ] Tulimshar's Legacy Quest
    * Red (Crimson) Knights and the Red Queen
    * Time Flask warp target
* [ ] Automatic Daily Event System (FY:DES)
    * Never repeat same event on same month (or maybe, same season)
    * Last 1~3 days
    * Excessive Randomness, specially on dialogs and settings ($@_$-abuse) for an unique experience

## Game Lore
* [x] Next Main Storyline movement
    * Monster King and the fragment hunt: Script control that
    * [ ] Main Continent must be finished and more mobs must be added before going to
    his Stronghold and attempt to take it down - design so a team of five players
    level 109 with Savior Set can handle the task.
* [x] Next Player Storyline movement
    * We'll start using Karma System for real, and give you the three legacies based
    on that and affected by player choice.
    * Next movements are building Karma points, using main storyline frame.
    * [x] Lua is a good NPC if you like the "open world" style, but add the guard NPC...
        * [x] And then, use this Guard NPC to sort player priorities
* [ ] Next City Development
    * Nivalis still needs to be finished with Angela and Blue Sage.
    * This will move city development to Frostia and Lilit next.

### Real Estate System
* [x] Add a house in LoF Village
* [x] Mirror that house multiple times
* [x] Add a house in Halinarzo too (cheapest ingame)
* [x] The castle in the Road
* [ ] Fairy house
* [x] Apartment Building
* [x] Sanitize rent prices

### Guild System
* [x] Guild House
* [x] Guild Storage NPC
* [ ] Guild Wars and great rewards

### Minigames
* [ ] Merchant Police Minigame
    * [ ] Strengthen lock: by melting an iron or copper ingot
    * Minigame to don't bust the lock (the right melting temperature)
    * Adds money to the safe and gets JOB EXPERIENCE (1% from needed to level up)
    * [ ] Run Bussiness: get a share from revenue and JExp
    * Similar to classic serve-clients-style minigame
    * Memorize client requests (take prints) - all time in the world
    * clear; is called and must select right option - small time frame
    * Super easy to cheat, but it is revenue based - must then wait 12 hours.
    * Might require a Quill - maybe?

### Balance System
* [ ] Review refine prices?

### Mapping
* [x] Saulc's Cave (#tmw2-dev March 5th)
* [ ] Pirate Cave
    * **OF COURSE** I mean TMW-BR Pirate's quest. And of course it is in hurscald.
    Saying more than that is spoilers, so I'll stop now. Unless Saulc have ideas.
* [ ] Imperial PVP Arena
    * Near Frostia, to elect the PVP King, see #admin
* [ ] Racing Maze
    * [ ] For Monsters
    * [ ] For Players and NPCs

### Magic Development
* [x] Bring Sagratha
    * Fill her to the brim with summon skills?
    * Make her tier 1
* [ ] Add Ranger Subclass
* [ ] Add Sniper Subclass
* [x] Add warning to untested subclass
* [ ] Lalica the Witch (reward: Hat)
* [x] Super Attack Boost fake-skill BERSERK (attack speed +100% SOME MALUS)
    * [ ] Add that skill somewhere “Divine Rage” (TMW2_DEMURE)

### Dungeon Development
* [ ] Party Dungeon Second Floor
    * Use setcells() and switches to close the wings
    * Add a step portal on the bottom of each wing. Player must be standing on it.
    * If a player is standing in every portal, all players get warped.
    A way to warp more is to everyone pile up at one portal, or to divide in equal-size
    teams
* [ ] Dungeon Fishing
    * Add more spots
    * Legendary Savior Set Blueprints
* [ ] We need a minecart in some dungeon (travel between cave chambers) in a Party Dungeon.
    * Or we could make it like Rush Game. Eh, board games are tricky.
* [ ] Global Boss
    * Defeating it could, as Saulc said, raise server EXP rates for a while.
    By setting $@EXP_EVENT to rad(5,15), `monster(); donpcevent "@exprate::OnPlayerCall"`.
* [ ] Heroes Hold
    * Traps should cause status effects at random too (these need client-data patch)
    * Add the missing levels for Crazyfefe if needed
* [ ] Grand Race
    * Objective is to reach first the other side of a maze - you're running against
    players AND NPCs (which have a set of random pathes they'll take). All equipment
    is allowed and skills are all green. GMs cannot participate.
    * This means if the race is hourly, even if nobody else is interested,
    there still are NPCs running and you may lose to them
    * You could bet Casino Coins too :3 Bet on your friend! Get more Casino Coins!

### LILIT, THE FAIRY ISLAND
* [x] The access bridge
* [x] The spider cave
    * must be mapped
    * spiders must be added
* [x] Boss Fight at the end (Level 40, multiplayer)
* Monsters
    * Alizarin Herb
    * Fairies
    * Green Dragons at middle
    * Mountain Snakes at bottom
* Quests and NPCs
    * [ ] Daily for Mountain Snake Egg
    * [ ] Permanent - Armor - for 40x Mountain Snake Skin
    * [ ] Fairy Pet - see Horcrux in BR for reference (haaaaaaaard, like Crazyfefe likes)
    * [x] Grand Hunter Quest - Mountain Snake (might take it while doing other quests)
    * [ ] Quest with Mountain Snake Tongue and Dragon Scales I guess?
    * [x] Leather Pants
* [ ] Lilit Central Tree
    * Player groups (lv 40) might want an audience with Lilit, thus, climing the tree.
    * They have to climb the stairs (ie. move upwards).
    Mobs are created as players progress. The mob is aggressive.
    * If players reach the top, they'll be brought to Lilit audience room.
    * [ ] Lilit challenge players to the Snake Pit if they want (like Crazyfefe Cave,
        but monster is fixed - Mountain Snake - with 1 extra snake every turn)
    Must kill all to advance round, and there's a reward at end (5+players*2 rounds?)
    It is a PARTY INSTANCE with warpparty().
    * [ ] There's also a General there. So you can challenge the Yetifly.
    * Real Estate booking in Lilit is managed here, of course.
    * Diplomacy is welcome

### FROSTIA, THE FROZEN KINGDOM
* [ ] More elves quests so you can enter while suffering racism.
    * Cindy quest should not be a pre-requisite.
* [ ] Assassin Set quests are ready - just copy paste
* [ ] Red Stockings
* [ ] AF King Arthur
* [ ] Replace Desert Bow by Elfic Bow and make it available.
* [ ] Challenge Frostia King to a duel with any PARTY. Causes a server cooldown.
    * No reward is provided. In other words, killing the king reward is suffice.
    * Or actually, we might give players some solid EXP boost, like a true boss.
* [ ] Other and any level 70+ content when appliable.

### Other places
* [ ] Redy's Volcano?
* [ ] Oceania?
* [ ] Orc Nomad Village? (Barbarians?)

#### Other Quests
* [ ] Inspector Quest (reward: Inspector Hat)
* [ ] Rock-Paper-Scissors (Halinarzo, probably casino)

#### Other items
* [ ] Jeans Chaps
* [ ] Monster Potion
* [ ] Setzer background story
* [ ] Fate's Potion
* [ ] Caffeinne - Cures Sleep (and ...?)
    * Item description: John H's favorite.

##### Missing Level 40~70 Equipment
* [ ] Brit Shield
* [ ] Bromenal Shield
    * Frostia, probably
* [ ] Setzer
    * Must learn about monster potion on Khafar
    * Monster potion is done at Nivalis Potion Shop
    * Must learn Setzer's Backstory «WHERE? WHICH ONE?»
    * Can be craft «WHERE? BY YOURSELF?»
* [ ] Bone Knife
* [ ] ????????
    * We need a new two hands weapon for level 87 and 105, low priority, just for record
* [ ] Warlord Boots
    * Part of Warlord Set - Nahrec?
* [ ] Jeans Chaps
    * Shouldn't be Pashua, given Canyon Difficulty (level 56)
    * Requires Jeans Shorts
* [ ] Silk Pants
    * Magic Equipment, Tulimshar
    * Baktar wants one, too
* [x] Chainmail Skirt
* [ ] Copper Armor
    * Part from Lieutenant set - but which one? Hmm, @Saulc could help perhaps?
* [ ] Warlord Plate
    * Part of the Warlord Set - Nahrec?
* [ ] Crusade Helmet
    * Colonel DUSTMAN is sitting in one!
* [x] Prsm Helmet
    * (Actually, shouldn't require Terranite Ore, Warlord Helmet, or be Spring only)
    * Paxel and his dialog are nice and dandy, but quest difficulty is stupid
* [ ] Warlord Helmet
    * Part of the Warlord Set - Nahrec?
* [ ] Viking Helmet
* [ ] Golden 4 Leaf Amulet
* [ ] Platinum 4 Leaf Amulet
    * Requires (previous 4l amulet) and a real 4 leaf clover this time (besides platyna)
    * Supreme boost, finally
* There's stuff on Art Repo which wasn't taken in account.
    * Lack of notes like this one means lack of plans properly recorded somewhere.

#### Minor
* [x] Minor dialog to Nivalis Town Well - this is the wrong well!
* [ ] Treasure Chests animation
* [ ] Add story books to Halinarzo Library
* [ ] Iilia Quest
    * [ ] Isbamuth
* [ ] Golden and Titan monsters
    * Stronger variation of common monsters with more rewards/harder to slay.
    * Golden is focused on drops, and Titan is focused on EXP.
* [ ] Discuss with Saulc about Nahrec the Heavy Armor Maker and about Warlord set
    * Bring the second helmet from Warlord Set from TMW-BR too, very good quest
* [ ] We could use real money...

