# Rules
Healing is in %, ranging from 1% to 90%.

## Formulas
  + MinHeal %: @rarity * ((@type*1) + 1)
  + MaxHeal %: @rarity * ((@type*2) + 1)
  + Delay: 1 + (@type*2)
      + Sweeties: 1s
      + Vegetables: 3s
      + Proteins: 5s
      + Proccessed: 7s
      + Magical: 9s


# Attributes
New healing system have two attributes:

##    @type
+          0 - Sweeties (lowest)
+          1 - Vegetables
+          2 - Proteins
+          3 - Proccessed highly chaloric
+          4 - Magical (highest)

##   @delay
      Overrides the lasting time

##   @rarity
      How rare the item is (how much should it effect). Ranges from 1 to 10.

+ 1 : Common Drop ( > 10%)
+ 2 : Shop item / Candor
+ 3 : Shop item / Tulim
+ 4 : Shop item / Hurns, Nival
+ 5 : 
+ 6 : 
+ 7 : 
+ 8 : 
+ 9 : Aeros Shop items
+ 10: 

# Quick Cheat copy-paste
```c
		@type = 1;
		@rarity=4;
```

```c
		doevent "legacy_heal::OnUse";
```

