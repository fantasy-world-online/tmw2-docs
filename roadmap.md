## TMW2: Moubootaur Legends Total Visualization

The information in this file might be incomplete or incorrect,
The TMW2 Development Team plan's for items.

**Related to TMW2 Client Data**

https://gitlab.com/TMW2/clientdata

**Related to TMW2 Art**

https://gitlab.com/TMW2/Art

:grey_question:: Unknown.
items are sort by levels

### Ammo ?/?
Name | Level | Location | Specification
---- |:-------:|:-----:|:--------:
Tolchi arrow | 1 |Tolchi candor shop| arrow made for beginner 
`Training arrow` |  | | 
`cursed arrow` |  | | 
`iron arrow` |  | | 
`Poison arrow` |  | | 
`Thorn arrow` |  | | 
`bullet` |  | | 
`Sacred Bullet` |  | | 
arrow |  | | 
`round stone` | 10 | Sell by Miners | Test
`bone arrow` | 70 | Craft by using bone
### Amulet 5/22
Name | Level | Location | Specification
---- |:-------:|:-----:|:--------:
Tooth Necklace | 8 |Saxso Chest| Item that give more damage but reduce player vita
Lifestone Pendant | 10 |Tulimshar| most basic regeneration pendant
Alvasus Pendant | 21 |Halin | A basic pendant 
`Jack necklace` | 26 |Halloween | 
`Dark Pendant` | 29 |Cave, Evil person | Drastically increase player def specially on PvP, but drains HP
`Silver Fourleaf` | 35 | LoF Village | Misnamed iron fourleaf, evolved from Crozenite
`Woolvern Necklace` | 39 | Nivalis, Frostia | Drop by wolvern, secret quest
Crozenite Four Amulet | 45 |LaJohanne | fourleaf +1 
`Blood Stone Pendant` | 50 |Hurnscald Shaman | Craftable with Blood Stone, increase damage and reduce agi
Mouboo Pendant | 55 |Halin church | a good regen equip, good bonus for mage too
`Infinit Collar` | 59 |Nivalis | 
`Claw Pendant` | 63 |Barbarian Village | Craftable with Wolvern Claw (Need stats)
`Bromenal Fourleaf` | 67 | | Not sure
`Raw Talisman` | 71 | | 
`Monk Pendant` | 75 | | Increase self heal and increase heal give to other players
`Barbarian Pendant` | 79 | | Wolvern Necklace +1
`Japonese Collar` | 83 | | 
`Golden Four Leaf` | 87 | | Silver Fourleaf +1
`Heart Necklace` | 91 | | 
`Master Claw Pendant` | 94 | | Barbarian Pendant +1
`Enchanter` | 98 | | 
`Flight talisman` | 103 | | 
`Angel Amulet` | 109 | | 
### Charm 4/25
Name | Level | Location | Specification
---- |:-------:|:-----:|:--------:
`Red, Purple,Blue, Green Broken Doll` | 13 | | A basic quest that give one of broekn doll, each one have different basic bonus
`Doll` | 20 | | 
`Red Stocking` | 25 |Nivalis Christmas | 
`Leather ball` | 26 |Pashua village, :grey_question: | Made of snake leather to increase accuracy
Four Leaf Clover | 30 |Drop from clover patch | critical defence, and base of item for leaf amulet
Zarkor Scroll | 31 | Zarkor in miner cave | Regen hp give more critical a,nd allow player to summon cave maggot
`Leather Quiver` | 40 | Hurnscald | Craftable
`Small Mecha medaillon` | 43 | LoF Village | 
`Astral cube` | 46 |Ausbel | 
`Forest Quiver` | 51 | | 
Demon Skull | 55 |Cave | Drop by Fire Skull
`Earth Scroll` | 60 | | 
`theta book` | 63 | | 
`Snake Quiver` | 66 | | 
Dragon Star | 70 | | Drop by dragon
`Old Towel` | 73 | | 
`Spectral Orb` | 75 | | 
`Grimoire` | 78 | | 
`Terranite Quiver` | 81 |Hurnscald |Craftable
`Demon Ash Urn` | 84 | | 
`Fire Scroll` | 87 | | 
`Heart of isis` | 90 | | 
`Mistical Book` | 93 | | 
`Platinium quiver` | 96 |Hurnscald | Craftable
`Golden Heart of isis` | 99 | | 
`Plush Mouboo` | 105 | | 
### Chest 9/44
Name | Level | Location | Specification
---- |:-------:|:-----:|:--------:
Creased Chirt | 1 | Lajohanne | basic armor
Candor Shirt | 4 | Candor | Basic armor +1
Short Tank Top | 8 |? | Currently sold for an overprice, should be craftable
T-neck sweater | 13 | Nivalis/Frostia | Christmas Item
Cotton Shirt | 15 | Tulim kid | basic armor +2, dyeable
Valantine Dress | 15 |  | Valentine Day
Silk Robe | 20 | Tulm Shop | Mage armor + dyeable
Leather Shirt | 21 | Drop by Black Scorpion | Basic armor +3
Community Shirt | 25 | LUA tulim | Help dev team to get it, dyeable item
Xmas Sweater | 26 |Christmas | 
`Sailors Shirt` | 29 |Ship | 
`V-neck Sweater` | 32 |Halin | 
Desert Shirt | 37 | Canyon | Basic armor +4 for leather shirt + snake skins
`Tank Top` | 42 |Mine | 
`Lieutant Armor` | 45 |Hurnscald | 
`Yeti Tank Top` | 45 |Nivalis | 
Chainmail | 45 | LoF | Craftable, heroes hold, reduce a bit move speed
`V-neck Jumper` | 49 | | 
Forest Armor | 50 | Hurns :grey_question: | Reward for killing bandit lord, good armor for archer
`Sanctum Robe` | 52 |Fairy village | 
`Justifier Chest` | 54 | | 
`Sorcerer Robe` | 55 | | Second Tier is level 50
`Copper Armor` | 57 |Halin | upgrade of Lieutnant armor
`Light Plate` | 60 | | 
`Golden Chainmail` | 55 | | 
`Alchemist Armor` | 67 |Lof Village | 
Warlord Plate | 70 | | Droppable
`Sanctum Robe` | 71 | | `2 sanctum robe ?`
`Bromenal Chest` | 73 | | 
`Golden Light Plate` | 74 | | 
`Crusader` | 76 | | 
`Assassin Chest` | 78 | | 
`Terranite Armor` | 80 | | Terranites are level 90 :o
`Red Knight` | 82 |Past Tulimshar | Ozthokk Time Travel quest
`Lazurite Robe` | 87 | |
`Black Mamba` | 89 | |
`Golden Warlord Plate` | 95 | | 
`Dragon Snake Plate` | 97 | | 
`Imperial Warlord` | 99 | | 
`Imperial Cape` | 100 | | 
`Demon Plate` | 103 | | 
`Savior Plate` | 109 | | 
### Feet 5/28
Name | Level | Location | Specification
---- |:-------:|:-----:|:--------:
Creased Boots | 3 | LaJohanne, Dan| Basic boots
Candor Boots | 7 |Candor | Basic boots +1 
Loosy Moccasins | 10 |Candor | Basic boots +2
Boots | 13 |Cave | Drop By red Slime
Tulimshar Guard Boots | 17 |Tulimshar, Dausen | Basic boots +3, give water to guard to get it 
Cotton Boots | 20 |Tulimshar | less def than tulimshar, increase a bit mana, need cotton cloth and silk coccon to get it
`Santa Boots` | 25 | Christmas | NYI
Squirrel Boots | 26 |Hurnscald, Woody | quest required, squirrel pelt and boots, increase move speed 
Leather Boots | 32 |Halin | 
Fur Boots | 39 |Nivalis, Traper house | need Boots, leather patch and a lot of white furs, good def, resist to ice element, fear fire 
Red Stocking | 45 | | 
`Black Cats Boots` | 50 | | TODO
Bromenal Boots | 55 | | 
`Cavaliero Boots` | 58 | | 
`Wizard Mocassins` | 61 | | Perfect for mages (cli missing)
`Black Boots` | 63 | | 
`Wolvern Boots` | 66 | | 
`Helios Boots` | 69 | | 
`Black Mamba Shoes` | 72 | | 
`Necromancer moccassins` | 76 | | 
`Terranite Boots` | 79 | | 
`Warlord Boots` | 84 | | 
`Assassin Boots` | 88 | | 
`Imperial Boots` | 90 | | 
`Witch Boots` | 94 | | Obtain from Lalica the Witch (cli missing)
`Golden Warlord Boots` | 96 | | 
`Demonic Boots` | 99 | | 
`Assassin boots` | 103 | | TODO
`Savior Boots` | 107 | | 
### Hands 5/24
Name | Level | Location | Specification
---- |:-------:|:-----:|:--------:
Creased Gloves | 1 | La Johanne, Chef Gado| Basic glove
Candor Gloves | 4 | Candor, Rosen | Basic gloves +1 
Cotton Gloves | 6 | Tulimshar | Silvia Quest
`Cotton Bands` | 9 |Tulimshar, | :grey_question: homeless guy required some food items
`Cave Snakeskin Bands` | 22 | Mine | Need to have cave snake skin to get it
Miner Gloves | 28 | Mine | Various items, basic gloves +2, drop by red slimes 
Armbands | 30 | | Mage Equipment
`Forest Gloves` | 34 |Hurnscald, Forest | Gloves increase bow accuracy and give dex bonus
Leather Gloves | 40 | LoF Village| Craftable for mountain snake skin, dropable on mountain snake
`Leather Bands` | 47 |Pashua Village, Halin | :grey_question:
Copper Armbands | 50 | | Mage Equipment
`Silk Gloves` | 60 |Hurnscald | Weak Gloves give a good magic bonus, obtain with toon of silk coccon and cotton gloves 
`Silver Armband` | 63 | | :grey_question:
`Bromenal Gloves` | 66 | | 
Iron armband | 70 | Hurns :grey_question: | Mage Equipment. Craftable in hurns with iron ingot and leather bands
`Golden armbands` | 70 | | :grey_question:
`Snake Armband` | 74 | | :grey_question:
`Mana Gloves` | 77 | | 
`Terranite Gloves` | 80 | | 
`Necromancer Gloves` | 84 | | 
`Dragon Gloves` | 87 | | 
`Warlord Gloves` | 90 | | 
`Sarab Armlet` | 93 | | Mage Equipment :grey_question:
`Golden Warlord Gloves` | 95 | | 
`Assassin Gloves` | 99 | | 
`Savior Gloves` | 105 | | 
### Legs 6/31
Name | Level | Location | Specification
---- |:-------:|:-----:|:--------:
Creased Short | 1 | La johanne, Chest| Basic Short
Candor short | 3 | Candor, Zegas| Basic short +1
Cotton Short | 13 |Tulimshar |Basic shorts, low magic bonus, dyeable 
Cotton Skirt | 15 | | Available at Shoppa Kep
`Mini Skirt` | 15 | | Sell, craft, these decorative items ya know
Pirate Short | 20 | Ocean| Dropable by pirates, increase damage
Farmer Pants | 21 |Tulimshar, Anwar | Basic pants +2
Jeans Shorts | 25 |Lof Village, marchant | Craftable for cave snake skin, dropable on cave snake 
`Santa Pants` | 26 |Nivalis, Christmas | 
Luffyx summer short | 31 | Hurnscald, Luffyx | Summer quest, required cocktail, basic short +3 
`Cotton Trousers` | 36 |Hurnscald | Inspecteur quest, Basic pant +5, dyeable 
`kilt` | 40 | | 
Raid Trousers | 36 | | Perfect to raid a dungeon with friends (HH?)
`Bandit Pants` | 45 |Cave | Dropable by Bandit, increase agi and dex
Leather Trousers | 48 | | Hali?
`Bande déchiré` | 51 | | 
`Black Jeans` | 53 | | 
Jeans Chaps | 56 | | Pashua?
Silk Pants | 59 | | Mage Equipment
Chainmail skirt | 64 | | 
`Bromenal pants` | 68 | | 
`Golden Chainmail Skirt` | 70 | | 
`Cavalier Pants` | 73 | | art
`Warlord Pants` | 75 | | 
`Terranite Pants` | 77 | |
`Red Knight` | 82 | | 
`Golden Terranite` | 83 | | 
`Red BR Trousers` | 86 | | 
`Black Mamba Pants` | 90 | | 
`Imperial Pants` | 98 | | 
`Savior Pants` | 108 | | 
### Headgear

Automatically Generated

Name | Level | Location | Specification
---- |:-------:|:-----:|:--------:
GM Cap|1| Tulimshar Alliance Room | GM Item
ADMIN Cap|1| @ item | ADM Item
DEV Cap|1| Lua | Contributor Reward
Magic Top Hat|1| @ item | Kill-The-Saulc event
Bunny Ears|2| Easter | Easter Rare Drop
`AFK Cap`|2| :grey_question: | 
`Pumpkin Hat`|4| :grey_question: | 
`Viking Helmet`|4| :grey_question: | 
`Centurion Helmet`|4| :grey_question: | 
`Corsair Hat`|4| :grey_question: | 
`Brimmed Feather Hat`|8| :grey_question: | 
`Brimmed Flower Hat`|8| :grey_question: | 
`Candle Helmet`|8| :grey_question: | 
`Eggshell Hat`|8| Easter | Easter Item
`Warlord Helmet`|8| :grey_question: | 
`Trapper Hat`|8| :grey_question: | 
`Yeti Mask`|8| :grey_question: | 
`Dragon Eggshell`|8| :grey_question: | 
`Opera Mask`|8| :grey_question: | 
`Axe Hat`|8| :grey_question: | LoF Merge
`Dark Knight Helmet`|8| :grey_question: | 
`Earmuffs`|8| Nivalis | 
`Samurai Helmet`|8| :grey_question: | 
`Imperial Crown`|8| :grey_question: | 
`Cap`|8| :grey_question: | 
`Top Hat`|8| :grey_question: | 
`Bowler Hat`|8| :grey_question: | 
`Captain Cap`|8| :grey_question: | 
Candor Head Band|9| :grey_question: | Quest Item
Murderer Crown|10|  | Kill-the-Saulc event Reward
`Bandana`|12| Candor | Quest Item
`Antlers Hat`|13| :grey_question: | 
`Santa Hat`|14| :grey_question: | 
Serf Hat|15| Tulimshar | Quest Item
Pinkie Hat|16| Hurnscald | Rare drop, bonus quest item
Right Eye Patch|16|  | Daily Login Reward Bonus (Summer)
Santa Beard Hat|18| :grey_question: | Christmas Gift
`Fancy Hat`|19| :grey_question: | 
Orange Eggshell Hat|20| Easter | Easter
Cave Snake Hat|20|  | Rare Drop
Fluffy Hat|22|  | Rare Drop
Green Eggshell Hat|23| Easter | Easter
`Desert Hat`|25| :grey_question: | 
`Pirate Bandana`|28| :grey_question: | 
Miner Hat|29| Tulimshar Mines | Quest Item
Knit Hat|29| Nivalis | Winter Quest
`Funky Hat`|30| :grey_question: | 
`Infantry Helmet`|30| :grey_question: | 
Dark Eggshell Hat|30| Easter | Easter
`Paper Bag`|30| :grey_question: | 
`Farmer Hat`|30| :grey_question: | 
Brimmed Hat|33| Tulimshar | Lucky Item - Monster Points
`Mouboo Hat`|33| :grey_question: | 
`Cat Ears`|33| :grey_question: | 
Bucket|36| Halinarzo | Quest Item
`Desert Helmet`|40| :grey_question: | 
`Graduation Cap`|40| :grey_question: | 
`Sailor Hat`|40| :grey_question: | 
`Shroom Hat`|40| :grey_question: | 
`Bromenal Helmet`|45| :grey_question: | 
Chef Hat|45| Halinarzo | Quest Item
`Bandit Hat`|45| :grey_question: | 
`Forest Shroom Hat`|45| :grey_question: | 
`Skull Mask`|50| :grey_question: | 
`Sacred Forest Hat`|50| :grey_question: | 
`Pink Helmet`|50| :grey_question: | 
`Leprechaun Hat`|50| :grey_question: | 
Fafi Mask|53| LoF | Quest Item
`Crusade Helmet`|55| :grey_question: | 
Prsm Helmet|60| LoF | Spring Quest
`Wicked Shroom Hat`|60| :grey_question: | 
`Alpha Mouboo Hat`|65| :grey_question: | 
`Moonshroom Hat`|70| :grey_question: | 
`Chemist Helmet`|80| :grey_question: | 
`Cleric Cap`|80| :grey_question: | 
`Bull`|90| :grey_question: | 
`Darkhelm`|90| :grey_question: | 
`NPC Blinking Eyes`|150|  | NPC Use Only
`Rice Hat`|150| :grey_question: | 


### Location

Please refer to wiki maps page

Name | Level | Id | Specification, Lore
---- |:-------:|:-----:|:--------:
La Johanne | 1 | 002| First place in game, basic quest and introduction, also used to travel cador to tulismhar.
Candor | 3 |005, 006 | First island ingame. Basic quests.
Tulimshar | 10 | 003, 004 | 
Miner Camp |  | 007|
Canyon |  | 010|
Halinarzo |  | 011|
`Pashua Village` |  | |
`Desert Graveyard` |  | |I prefer to bury every dead people on Swamps. But pyramids are welcome. (Sarab - BR)
Hurnscald |  |012, 013, 014, 015|
Lof Village |  |017, 018|
Nivalis |  |019|
Frostia |  | |020?|
`Fairy Land` |  | |018-5|
Sincerity Island |  |018| 
Fafi Island |  |018|Called Secret Island
`Catz Cave` |  | |
`Graveyard` |  | |Eternal Swamps
`Egiptain Templare` |  | |
`Luvia House` |  | | 
Diamond Cove |  |017-3| 
Mouboo Church |  | |
Bandit Cave |  |015-2|
Magic School  |  | | 
JesusAlva Castle  |  | | 
`Jail`  |  |sec_pri| Map ID cannot be changed.
`Crypt`  |  | |
`Terranite Cave`  |  |015-6|
`Santa House`  |  | |
Easter Forest  |  | |
Aeros  |  |001-1| Gm Event place
`Tulimshar Sewer`  |  |003-1-1|
